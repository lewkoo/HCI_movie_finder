﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;

namespace Proj.Class
{
    public class Movie: INotifyPropertyChanged
    {
        private String _title = null;
        private int _year = -1;
        private int _lengthInMin = -1;
        private String _certification = null;
        private int _rating = -1;
        private ObservableCollection<Genre> _genres = new ObservableCollection<Genre>();
        private ObservableCollection<String> _actors = new ObservableCollection<String>();
        private String _director;

        #region Property
        [XmlElement("title")]
        public String Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Missing title");
                _title = value;
                OnPropertyChanged("Title");
            }
        }

        [XmlElement("year")]
        public int Year
        {
            get { return _year; }
            set
            {
                if (value == -1)
                    throw new ArgumentOutOfRangeException("Missing Year information");
                _year = value;
                OnPropertyChanged("Year");
            }
        }

        public int LengthInMinutes
        {
            get { return _lengthInMin; }
            set
            {
                if (value == -1)
                    throw new ArgumentOutOfRangeException("Missing MovieLength Information");
                _lengthInMin = value;
                OnPropertyChanged("Length");
            }
        }

        [XmlElement("length")]
        public String MovieLengthInfo
        {
            get
            {
                //convert back to movie length format
                return LengthInMinutes + " " + "min";
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Missing MovieLengthInfo");
                //need to retrieve integer data from <length> tag, use regular expression to retrieve it
                string minutes = Regex.Match((string)value, @"\d+").Value;
                LengthInMinutes = Int32.Parse(minutes);
                OnPropertyChanged("LengthInMinutes");
            }
        }

        //not every movie have certificate information, so it can be null
        [XmlElement("certification", IsNullable = true)]
        public String Certification
        {
            get { return _certification; }
            set
            {
                _certification = value;
                OnPropertyChanged("Certification");
            }
        }

        [XmlElement("rating")]
        public int Rating
        {
            get { return _rating; }
            set
            {
                if (value == -1)
                    throw new ArgumentOutOfRangeException("Missing Rating");
                _rating = value;
                OnPropertyChanged("Rating");
            }
        }

        [XmlElement("genre")]
        public ObservableCollection<Genre> Genres
        {
            get { return _genres; }
            set
            {
                _genres = value;
                OnPropertyChanged("Genres");
            }
        }

        [XmlElement("actor")]
        public ObservableCollection<String> Actors
        {
            get { return _actors; }
            set
            {
                _actors = value;
                OnPropertyChanged("Actors");
            }
        }

        [XmlElement("director")]
        public String Director
        {
            get { return _director; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Missing Director");
                _director = value;
                OnPropertyChanged("Director");
            }
        }
        #endregion

        public Movie()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
