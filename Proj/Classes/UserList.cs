﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace Proj.Class
{
    [XmlRoot("userlist")]
    public class UserList
    {
        private ObservableCollection<User> _userList = new ObservableCollection<User>();
        [XmlElement("User")]
        public ObservableCollection<User> UsersList
        {
            get { return _userList; }
            set
            {
                if (value != null)
                {
                    _userList = value;
                }
            }
        }

        public UserList()
        {
        }

        public void addUser(User toAdd)
        {
            UsersList.Add(toAdd);
        }

    }
}
