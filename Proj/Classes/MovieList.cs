﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Windows;
using System.ComponentModel;
using Proj.Class;

namespace Proj.Class
{
   
    [XmlRoot("movielist")]
    public class MovieList
    {
        private ObservableCollection<Movie> _movieListInstance = new ObservableCollection<Movie>();
        [XmlElement("movie")]
        public ObservableCollection<Movie> MovieListInstance
        {
            get { return _movieListInstance; }
            set
            {
                if (value != null)
                {
                    _movieListInstance = value;
                }
            }
        }

        public MovieList()
        {
        }
    }
}
