﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Proj.Class
{
    public enum Genre
    {
        None,
        All,
        [XmlEnum(Name = "Action")]
        Action,
        [XmlEnum(Name = "Adventure")]
        Adventure,
        [XmlEnum(Name = "Animation")]
        Animated,
        [XmlEnum(Name = "Biography")]
        Biography,
        [XmlEnum(Name = "Comedy")]
        Comedy,
        [XmlEnum(Name = "Crime")]
        Crime,
        [XmlEnum(Name = "Documentary")]
        Documentary,
        [XmlEnum(Name = "Drama")]
        Drama,
        [XmlEnum(Name = "Family")]
        Family,
        [XmlEnum(Name = "Fantasy")]
        Fantasy,
        [XmlEnum(Name = "Film-Noir")]
        Film_Noir,
        [XmlEnum(Name = "History")]
        History,
        [XmlEnum(Name = "Horror")]
        Horror,
        [XmlEnum(Name = "Musical")]
        Musical,
        [XmlEnum(Name = "Mystery")]
        Mystery,
        [XmlEnum(Name = "Romance")]
        Romance,
        [XmlEnum(Name = "Sci-Fi")]
        Sci_Fi,
        [XmlEnum(Name = "Sport")]
        Sport,
        [XmlEnum(Name = "Thriller")]
        Thriller,
        [XmlEnum(Name = "War")]
        War,
        [XmlEnum(Name = "Western")]
        Western,
        [XmlEnum(Name = "Adult")]
        Adult,
        [XmlEnum(Name = "Reality-TV")]
        Reality_TV,
        [XmlEnum(Name = "Short")]
        Short
    }
}
