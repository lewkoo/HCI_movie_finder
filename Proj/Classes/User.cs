﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Proj.Class
{
    public class User
    {
        private String _userID = null;
        [XmlElement("UserID")]
        public String UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }


        private String _password = null;
        [XmlElement("Password")]
        public String Password
        {
            get { return _password; }
            set { _password = value; }
        }


        private MovieList _recentlyViewed = new MovieList();
        [XmlElement("RecentlyViewed", IsNullable=true)]
        public MovieList RecentlyViewed
        {
            get { return _recentlyViewed; }
            set { _recentlyViewed = value; }
        }


        private MovieList _bookmarkedMovies = new MovieList();
        private string currUserName;
        private string currPassword;
        [XmlElement("BookmarkedMovies", IsNullable = true)]
        public MovieList BookmarkedMovies
        {
            get { return _bookmarkedMovies; }
            set { _bookmarkedMovies = value; }
        }

        public User()
        {

        }

        public User(string currUserName, string currPassword)
        {
            // TODO: Complete member initialization
            this.UserID = currUserName;
            this.Password = currPassword;
        }



        

    }
}
