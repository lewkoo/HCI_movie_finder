﻿namespace Proj
{
    partial class MovieDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.movieTitle = new System.Windows.Forms.TextBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.yearLaber = new System.Windows.Forms.Label();
            this.year = new System.Windows.Forms.TextBox();
            this.lengthLabel = new System.Windows.Forms.Label();
            this.length = new System.Windows.Forms.TextBox();
            this.ratingLabel = new System.Windows.Forms.Label();
            this.ratingGauge = new System.Windows.Forms.ProgressBar();
            this.ratingInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // movieTitle
            // 
            this.movieTitle.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.movieTitle.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.movieTitle.Location = new System.Drawing.Point(39, 3);
            this.movieTitle.Name = "movieTitle";
            this.movieTitle.ReadOnly = true;
            this.movieTitle.Size = new System.Drawing.Size(129, 20);
            this.movieTitle.TabIndex = 0;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Location = new System.Drawing.Point(3, 6);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(30, 13);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "Title:";
            // 
            // yearLaber
            // 
            this.yearLaber.AutoSize = true;
            this.yearLaber.Location = new System.Drawing.Point(3, 32);
            this.yearLaber.Name = "yearLaber";
            this.yearLaber.Size = new System.Drawing.Size(32, 13);
            this.yearLaber.TabIndex = 2;
            this.yearLaber.Text = "Year:";
            // 
            // year
            // 
            this.year.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.year.Location = new System.Drawing.Point(39, 29);
            this.year.Name = "year";
            this.year.ReadOnly = true;
            this.year.Size = new System.Drawing.Size(129, 20);
            this.year.TabIndex = 3;
            // 
            // lengthLabel
            // 
            this.lengthLabel.AutoSize = true;
            this.lengthLabel.Location = new System.Drawing.Point(3, 58);
            this.lengthLabel.Name = "lengthLabel";
            this.lengthLabel.Size = new System.Drawing.Size(43, 13);
            this.lengthLabel.TabIndex = 4;
            this.lengthLabel.Text = "Length:";
            // 
            // length
            // 
            this.length.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.length.Location = new System.Drawing.Point(52, 55);
            this.length.Name = "length";
            this.length.ReadOnly = true;
            this.length.Size = new System.Drawing.Size(116, 20);
            this.length.TabIndex = 5;
            // 
            // ratingLabel
            // 
            this.ratingLabel.AutoSize = true;
            this.ratingLabel.Location = new System.Drawing.Point(3, 84);
            this.ratingLabel.Name = "ratingLabel";
            this.ratingLabel.Size = new System.Drawing.Size(41, 13);
            this.ratingLabel.TabIndex = 6;
            this.ratingLabel.Text = "Rating:";
            // 
            // ratingGauge
            // 
            this.ratingGauge.Location = new System.Drawing.Point(52, 84);
            this.ratingGauge.Maximum = 9;
            this.ratingGauge.Name = "ratingGauge";
            this.ratingGauge.Size = new System.Drawing.Size(85, 13);
            this.ratingGauge.Step = 1;
            this.ratingGauge.TabIndex = 7;
            // 
            // ratingInfo
            // 
            this.ratingInfo.AutoSize = true;
            this.ratingInfo.Location = new System.Drawing.Point(144, 84);
            this.ratingInfo.Name = "ratingInfo";
            this.ratingInfo.Size = new System.Drawing.Size(0, 13);
            this.ratingInfo.TabIndex = 8;
            // 
            // MovieDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.ratingInfo);
            this.Controls.Add(this.ratingGauge);
            this.Controls.Add(this.ratingLabel);
            this.Controls.Add(this.length);
            this.Controls.Add(this.lengthLabel);
            this.Controls.Add(this.year);
            this.Controls.Add(this.yearLaber);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.movieTitle);
            this.Name = "MovieDisplay";
            this.Size = new System.Drawing.Size(180, 127);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox movieTitle;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label yearLaber;
        private System.Windows.Forms.TextBox year;
        private System.Windows.Forms.Label lengthLabel;
        private System.Windows.Forms.TextBox length;
        private System.Windows.Forms.Label ratingLabel;
        private System.Windows.Forms.ProgressBar ratingGauge;
        private System.Windows.Forms.Label ratingInfo;
    }
}
