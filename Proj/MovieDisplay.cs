﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Proj.Class;

namespace Proj
{
    public partial class MovieDisplay : UserControl
    {

        private Movie currentMovie;

        public Movie CurrentMovie
        {
            get { return currentMovie; }
            set { currentMovie = value; }
        }

        public MovieDisplay()
        {
            InitializeComponent();

            int numStepsPerf = 7;
            for (int i = 0; i < 6; i++)
            {
                ratingGauge.PerformStep();
            }

            ratingInfo.Text = "7";


        }

        public MovieDisplay(Movie givenMovie)
        {
            InitializeComponent();

            titleLabel.Text = givenMovie.Title;
            year.Text = givenMovie.Year.ToString();
            length.Text = givenMovie.LengthInMinutes.ToString();
            int rating = givenMovie.Rating;
            for (int i = 0; i < rating; i++)
            {
                ratingGauge.PerformStep();
            }
            ratingInfo.Text = rating.ToString();
        }

        
    }
}
