﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Proj
{
    public partial class Form3 : Form
    {
        bool userRegistered = false;

        public bool UserRegistered
        {
            get { return userRegistered; }
            set { userRegistered = value; }
        }



        private Class.UserList Users;

        public Class.UserList GetModifiedUserList
        {
            get { return Users; }
            set { Users = value; }
        }
        private Class.User selectedUser;

        public Class.User SelectedUser
        {
            get { return selectedUser; }
            set { selectedUser = value; }
        }


        public Form3()
        {
            InitializeComponent();
            userName.Enabled = true;
            password.Enabled = false;
            reqOne.Enabled = false;
            reqTwo.Enabled = false;
        }

        public Form3(Class.UserList Users)
        {
            // TODO: Complete member initialization
            this.Users = Users;
            InitializeComponent();
            userName.Enabled = false;
            password.Enabled = false;
            reqOne.Enabled = false;
            reqTwo.Enabled = false;
        }


        private void login_Click(object sender, EventArgs e)
        {

            String currUserName = null;
            String currPassword = null;

            if (usrName.Text.Length != 0 && pWord.Text.Length != 0)
            {
                currUserName = usrName.Text.ToString();
                currPassword = pWord.Text.ToString();

                bool foundID = false;

                for (int i = 0; i < Users.UsersList.Count && !foundID; i++)
                {
                    if (Users.UsersList[i].UserID.Equals(currUserName))
                    {
                        if (Users.UsersList[i].Password.Equals(currPassword))
                        {

                            selectedUser = Users.UsersList[i];
                            foundID = true;
                            //login success, close the form
                            this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        }

                        else
                            MessageBox.Show("Wrong password. Please try again.");
                    }
                    
                }

                if (foundID == false)
                {
                    MessageBox.Show("Provided User ID isn't registered. Please, check you user ID and try again");
                }

            }
            else
            {
                MessageBox.Show("Please provide User ID");
            }

            
            

        }


        private void Form3_KeyDown(object sender, KeyEventArgs e)
        {
            //
            // Detect the KeyEventArg's key enumerated constant.
            //
            if (e.KeyCode == Keys.Enter)
            {
                MessageBox.Show("You pressed enter! Good job!");
            }
            else if (e.KeyCode == Keys.Escape)
            {
                MessageBox.Show("You pressed escape! What's wrong?");
            }
        }
       

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void register_Click(object sender, EventArgs e)
        {
            String currUserName = null;
            String currPassword = null;

            if (usrName.Text.Length != 0 && pWord.Text.Length != 0)
            {
                currUserName = usrName.Text.ToString();
                currPassword = pWord.Text.ToString();

                String foundID = null;
                

                for (int i = 0; i < Users.UsersList.Count && !userRegistered; i++)
                {
                    if (Users.UsersList[i].UserID.Equals(currUserName))
                    {
                        MessageBox.Show("There is a user already registered with given User ID. Please choose a different User ID and try again");
                    }
                    else
                    {
                        selectedUser = new Proj.Class.User(currUserName,currPassword);
                        Users.addUser(selectedUser);

                        ///register success, close the form
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        userRegistered = true;
                    }
                }

            }
            else
            {
                MessageBox.Show("Please provide User ID");
            }

            

        }
    }
}
