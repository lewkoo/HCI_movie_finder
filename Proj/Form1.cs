﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Proj.Class;
using System.Windows;
using System.IO;
using System.Configuration;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.Xml;
using System.Diagnostics;
using Proj.Properties;

namespace Proj
{
    public partial class Form1 : Form, INotifyPropertyChanged
    {
        bool searchExtra;
        Form2 advSearch;
        Form3 logIn;
        Form4 movie;


        User currentUser = null;

        string moviesSource = Path.Combine(Directory.GetCurrentDirectory() + "\\movies.xml");
        string usersSource = Path.Combine(Directory.GetCurrentDirectory() + "\\users.xml");
        [XmlElement("movieList", typeof(MovieList))]
        private MovieList _movies = new MovieList();
        public MovieList Movies
        {
            get { return _movies; }
            set
            {
                if(value != null)
                {
                    _movies = value;
                    OnPropertyChanged("Movies");
                }
            }
        }

        [XmlElement("userList",typeof(UserList))]
        private UserList _users = new UserList();
        public UserList Users
        {
            get { return _users; }
            set 
            {
                if (value != null)
                {
                    _users = value;
                    OnPropertyChanged("Users");
                }
                
            }
        }


        public Form1()
        {
            searchExtra = false;
            advSearch = new Form2();
            logIn = new Form3();
            movie = new Form4();
            InitializeComponent();
            tableLayoutPanel4.Hide();

            GetMovies();
            GetUsers();
            SetUpRecentMovies();


        }


        #region XmlMethods

        private void GetMovies()
        {
            Movies = (MovieList)DeserializeXML("movielist", typeof(MovieList), moviesSource);
        }

        private void GetUsers()
        {
            Users = (UserList)DeserializeXML("userlist", typeof(UserList), usersSource);
        }

        private object DeserializeXML(string elementName, Type type, string fileLocation)
        {
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = elementName;
            XmlSerializer serializer = new XmlSerializer(type, xRoot);

            XmlReader reader = XmlReader.Create(fileLocation);
            var toReturn = serializer.Deserialize(reader);
            reader.Close();
            return toReturn;

        }

        private void SerializeToFile(object toSerialize, string location)
        {
            using (StreamWriter stream = new StreamWriter(location)) 
            {
                XmlSerializer serializer = new XmlSerializer(toSerialize.GetType());
                serializer.Serialize(stream, toSerialize);
            }
        }

        #endregion



        #region ActionEvents

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            tableLayoutPanel1.Hide();
            tableLayoutPanel2.Hide();
            tableLayoutPanel4.Show();
            this.Size = new System.Drawing.Size(700, 517);
        }

        private void advancedSearch_Click(object sender, EventArgs e)
        {
            if (searchExtra == false)
            {
                advSearch.Show();
                searchExtra = true;
            }
            else
            {
                advSearch.Hide();
                searchExtra = false;
            }

        }

        private void logBtn_Click(object sender, EventArgs e)
        {
            logIn = new Form3(Users);
            //logIn.Show();
            var dialogResult = logIn.ShowDialog();
            if (dialogResult != System.Windows.Forms.DialogResult.Cancel)
            {
                currentUser = logIn.SelectedUser;
 
                if(logIn.UserRegistered == true) //if we have a new user/users registered
                Users = logIn.GetModifiedUserList;
            }

            

        }

        private void logButton_Click(object sender, EventArgs e)
        {
            logIn = new Form3(Users);
            logIn.Show();
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox2_MouseClick(object sender, MouseEventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox15_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            movie = new Form4();
            movie.Show();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void rightArrow_Click(object sender, EventArgs e)
        {
        

        }

        protected virtual void timer1_Tick(object sender, EventArgs e)
        {
            lblDisplay.Text = DateTime.Now.ToLongTimeString();
        }

        #endregion


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName) 
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            SerializeToFile(Movies, moviesSource);
            SerializeToFile(Users, usersSource);
        }

        private void SetUpRecentMovies()
        {
            movieDisplay1.Hide();
            movieDisplay2.Hide();
            movieDisplay3.Hide();
            movieDisplay4.Hide();

            //pull out 4 most recent movies, i.e. do a search. google first before doing anything that requires searching.

            ObservableCollection<Movie> recentMovies = new ObservableCollection<Movie>;




        }
    }
}
