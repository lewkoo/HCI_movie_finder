﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Proj
{
    public partial class Form4 : Form
    {
        Form3 logIn;
        public Form4()
        {
            logIn = new Form3();
            InitializeComponent();
        }

        private void retToPrevSearch_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void logBtn_Click(object sender, EventArgs e)
        {
            logIn = new Form3();
            logIn.Show();
        }

        private void search_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void logButton_Click(object sender, EventArgs e)
        {
            logIn = new Form3();
            logIn.Show();
        }
    }
}
